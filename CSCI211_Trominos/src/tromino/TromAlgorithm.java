package tromino;

import trominoMVC.TromController;

public class TromAlgorithm implements Runnable{
	private int myX, myY, thisBoxSize, myXBoard, myYBoard, boardY;
	private TromController myController;

	public TromAlgorithm(int boxSize, int xBoard, int yBoard, int missingX, int missingY, TromController controller) throws InterruptedException
	{
		myController = controller;
		thisBoxSize = boxSize;
		myX = missingX;
		myY = missingY;
		boardY = myController.myBoxSize - missingY -1;
		myXBoard = xBoard;
		myYBoard = yBoard;
		
		if (myController.checkIfBoxEmpty()) myController.myBox[myX][boardY] = true;
	}

	public void generateTroms() throws InterruptedException
	{
		
		try 
		{
			
			Thread.sleep(300);
		int halfSize = thisBoxSize / 2;

		if (thisBoxSize == 2) {

			//missing square in left half
			if (myXBoard == myX)
			{
				//missing square in lower left
				if(myYBoard == myY)
				{
					myController.myBox[myX + 1][boardY] = true;
					myController.myBox[myX+1][boardY-1] = true;
					myController.myBox[myX][boardY-1] = true;
				} 
				//missing square in upper left
				else {
					myController.myBox[myX +1][boardY] = true;
					myController.myBox[myX+1][boardY+1] = true;
					myController.myBox[myX][boardY+1] = true;
				}
			} 
			//missing square in right half
			else {

				//missing square in lower right
				if(myYBoard == myY)
				{
					myController.myBox[myX-1][boardY] = true;
					myController.myBox[myX][boardY-1] = true;
					myController.myBox[myX-1][boardY-1] = true;
				} 
				//missing square in upper right
				else {
					myController.myBox[myX - 1][boardY+1] = true;
					myController.myBox[myX][boardY+1] = true;
					myController.myBox[myX-1][boardY] = true;
				}

			}
			myController.displayBoxes();
			return;
		}

		int xCenter = myXBoard + halfSize;
		int yCenter = myYBoard + halfSize;

		int xUpperLeft, yUpperLeft, xUpperRight, yUpperRight, xLowerRight, yLowerRight, xLowerLeft, yLowerLeft;

		//if missing square is in left half
		if(myX < xCenter)
		{
			xUpperRight = xLowerRight = xCenter;
			yUpperRight = yCenter;
			yLowerRight = yCenter - 1;

			//if missing square is in lower left quadrant
			if (myY < yCenter)
			{
				//print out tromino
				myController.myBox[xCenter][myController.myBoxSize - yCenter -1] = true;
				myController.myBox[xCenter - 1][myController.myBoxSize -yCenter -1] = true;
				myController.myBox[xCenter][myController.myBoxSize - yCenter] = true;

				xUpperLeft = xCenter - 1;
				yUpperLeft = yCenter;
				xLowerLeft = myX;
				yLowerLeft = myY;
			} 
			//if missing square is in upper left quadrant
			else {
				//print out tromino
				myController.myBox[xCenter][myController.myBoxSize - yCenter] = true;
				myController.myBox[xCenter][myController.myBoxSize -yCenter -1] = true;
				myController.myBox[xCenter-1][myController.myBoxSize -yCenter] = true;

				xUpperLeft = myX;
				yUpperLeft = myY;
				xLowerLeft = xCenter - 1;
				yLowerLeft = yCenter - 1;
			}
		}
		//if missing square is in right half
		else {
			xUpperLeft = xLowerLeft = xCenter - 1;
			yUpperLeft = yCenter;
			yLowerLeft = yCenter -1;

			//if missing square is in lower right quadrant
			if(myY < yCenter){
				//print out tromino
				myController.myBox[xCenter][myController.myBoxSize -yCenter -1] = true;
				myController.myBox[xCenter - 1][myController.myBoxSize -yCenter -1] = true;
				myController.myBox[xCenter-1][myController.myBoxSize -yCenter] = true;

				xUpperRight = xCenter;
				yUpperRight = yCenter;
				xLowerRight = myX;
				yLowerRight = myY;
			}
			//if missing square is in upper right quadrant
			else {
				//print out tromino
				myController.myBox[xCenter-1][myController.myBoxSize -yCenter] = true;
				myController.myBox[xCenter - 1][myController.myBoxSize -yCenter -1] = true;
				myController.myBox[xCenter][myController.myBoxSize -yCenter] = true;

				xUpperRight = myX;
				yUpperRight = myY;
				xLowerRight = xCenter;
				yLowerRight = yCenter -1;
			}
		}


		

		myController.displayBoxes();
		
		
		//public TromAlgorithm(int boxSize, int xBoard, int yBoard, int missingX, int missingY, TromController myController)
		//tile out the four subboards
		TromAlgorithm trom1 = new TromAlgorithm(halfSize, myXBoard, myYBoard + halfSize, xUpperLeft, yUpperLeft, myController);
		trom1.generateTroms();
		TromAlgorithm trom2 = new TromAlgorithm(halfSize, myXBoard + halfSize, myYBoard + halfSize, xUpperRight, yUpperRight, myController);
		trom2.generateTroms();
		TromAlgorithm trom3 = new TromAlgorithm(halfSize, myXBoard + halfSize, myYBoard, xLowerRight, yLowerRight, myController);
		trom3.generateTroms();
		TromAlgorithm trom4 = new TromAlgorithm(halfSize, myXBoard, myYBoard, xLowerLeft, yLowerLeft, myController);
		trom4.generateTroms();
		} catch(InterruptedException e)
        {
        	System.out.println("Customer generator suspended.");
        }
	}


	public int getMyX() {
		return myX;
	}

	public int getMyY() {
		return myY;
	}

	public int getMyBoxSize() {
		return thisBoxSize;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}	

}
