package trominoMVC;



import java.awt.MouseInfo;

import tromino.TromAlgorithm;
import javax.swing.Timer;
/**
 * Controls the interaction between view and the service queue manager
 * @author hudson
 */
public class TromController implements Runnable
{
	public boolean[][] myBox;
	public int myBoxSize;
	private TromAlgorithm myModel;
	private TromView myView;
	private boolean mySuspended = true;
	private Thread myThread;
	
	
	/**
	 * Constructor
	 * @author hudson
	 */
	public TromController()
	{
		myView = new TromView(this);
		
		
		myThread = new Thread(this);
		mySuspended = false;
		this.start();
	}
	
	/**
	 * Calls on the view to display the customers of the given queue
	 * @author hudson
	 * @param queue
	 */
	public void displayBoxes()
	{
		
		myView.setBoxes();
	}
	
	/**
	 * Running this thread updates the view as well as allows most of the interaction to take place
	 * @author hudson
	 */
	public void run()
    {
    	try
    	{
    		synchronized(this)
    		{
    			this.updateView();
    		}
    	}
    	catch (InterruptedException e)
    	{
    		System.out.println("Thread suspended.");
    	}
    }
	
	/**
	 * Continuously updates and displays the running threads from the model (Service Queue Manager)
	 * @author hudson
	 * @throws InterruptedException
	 */
	private void updateView()throws InterruptedException
	{
		while(true)
		{
			this.waitWhileSuspended();
			try 
			{				
					if (myModel != null) myModel.generateTroms();
					this.displayBoxes();
					myModel = null;
					//this.screenWipe();
					myView.resetView();
					this.suspend();
			}
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * Causes the thread to wait while the boolean mySuspended is true
	 * @author hudson
	 * @throws InterruptedException
	 */
	private synchronized void waitWhileSuspended() throws InterruptedException
    {
    	while (mySuspended)
    	{
    		this.wait();
    	}
    }
    
	/**
	 * Sets mySuspended to true as well as suspends all threads from the queue manager
	 * @author hudson
	 */
    public void suspend()
    {
    	mySuspended = true;

    }
    
    /**
     * Starts  this thread
     * @author hudson
     */
    public void start()
    {
        try
        {
            myThread.start();
            mySuspended = true;
        }
        catch(IllegalThreadStateException e)
        {
            System.out.println("Thread already started");
        }
    }
    
    /**
     * Resumes this thread as well as all the thread in Service Queue Manager
     * @author hudson
     */
    public synchronized void resume()
    {
    	mySuspended = false;
    	this.notify();
    }
    
    /**
     * Pauses the threads when mySuspended = false, resumes the threads when it equals true
     * @author hudson
     * @throws InterruptedException 
     */
    public void startPause() throws InterruptedException
    {
    	
    	
    	if (myModel == null)
    	{
    		//this.screenWipe();
    		if (myView.getBoxSize() != -1 && myView.getThisX() != -1 && myView.getThisY() != -1)
    		{	
    			
    			
    			myBox = new boolean[myView.getBoxSize()][myView.getBoxSize()];
    		
    			for (int i = 0; i < myView.getBoxSize(); i++)
    			{
    				for (int j = 0; j < myView.getBoxSize();j++)
    				{
    					myBox[i][j] = false;
    				}
    			}
    			myBoxSize = myView.getBoxSize();
    			myView.setDisplaySize();
    			
    			myModel = new TromAlgorithm(myBoxSize, 0,0,myView.getThisX(), myView.getThisY(), this);
    			this.displayBoxes();
    			this.resume();
    	    	myView.changeStartPause();
    		}

    	}
    	myView.changeStartPause();
    }
    

    private void screenWipe() {
    	for (int i = 0; i < myBoxSize; i++)
		{
			for (int j = 0; j < myBoxSize; j++)
			{
				myBox[i][j] = false;
			}
		}
    	
    	this.displayBoxes();
    	myView.changeStartPause();
    	myView.resetView();
	}

	/**
     * returns the model
     * @return
     */
    public TromAlgorithm getModel()
    {
    	return myModel;
    }

    /**
     * The main function of the controller, starts everything else
     * @author hudson
     * @param args
     */
	public static void main(String[] args)
	{
		new TromController();
	}
	
	public boolean checkIfBoxEmpty()
	{
		for (int i = 0; i < myBoxSize; i++)
		{
			for (int j = 0; j < myBoxSize; j++)
			{
				if (myBox[i][j]) return false;
			}
		}
		
		return true;
	}
	
	public boolean checkIfBoxFull()
	{
		for (int i = 0; i < myBoxSize; i++)
		{
			for (int j = 0; j < myBoxSize; j++)
			{
				if (!myBox[i][j]) return false;
			}
		}
		
		return true;
	}
}
