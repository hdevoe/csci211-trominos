package trominoMVC;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.lang.reflect.Method;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import tromino.TromAlgorithm;


/**
 * The SimView Class displays the simulation and all accompanying data as well as allows for starting
 *  and pausing the controller via buttonListener
 * @author hudson
 *
 */
@SuppressWarnings("serial")
public class TromView extends JFrame
{
	//Constants
	private final int BOX_SIZE = 64;
	private final String EMPTY_BOX_IMG = "assets/emptySquare.png";
	private final String FULL_BOX_IMG = "assets/fullSquare.png";

	//Data Members
	private Image myScaledEmptyBox;
	private Image myScaledFullBox;
	private TromController myController;
	private Container myContentPane;
	private ButtonListener myStartPauseListener;
	private JLabel [][] myBoxes;
	private JButton myStartPauseButton;
	private JPanel mySimPanel;
	private JPanel myToolbar;
	private JPanel mySidebar;
	private JTextArea myOverallDisplay;
	private JTextArea[] myCashierDisplays;
	private JPanel myOverSimPanel;
	
	private JTextField mySetBoardSizeName = new JTextField("SET BOX SIZE (POWER OF 2): ");
	private JTextField mySetBoardSizeField = new JTextField(null);
	private JTextField mySetEmptyXName = new JTextField("SET EMPTY TILE X COORDINATE (STARTING AT 0): ");
	private JTextField mySetEmptyXField = new JTextField(null);
	private JTextField mySetEmptyYName = new JTextField("SET EMPTY TILE Y COORDINATE (STARTING AT 0): ");
	private JTextField mySetEmptyYField = new JTextField(null);
	
	private Font myFont = new Font("Helvetica", Font.PLAIN, 12);
	private Font myFont2 = new Font("Helvetica", Font.PLAIN, 10);
	private Font myFont3 = new Font("Helvetica", Font.BOLD, 15);
	
	Image emptyBox = Toolkit.getDefaultToolkit().getImage(EMPTY_BOX_IMG);
	Image fullBox = Toolkit.getDefaultToolkit().getImage(FULL_BOX_IMG);
	
	
	//Constructor
	
	/**
	 * Constructor that creates the view.
	 * 
	 * @param controller the SimulationController that gives function to the buttons.
	 * @author hudson
	 */
	public TromView(TromController controller)
	{
		
		
		
		
		
		
		
		
		//myScaledCounter = counter.getScaledInstance((int)(CUSTOMER_WIDTH * 1.15),(int)(CUSTOMER_HEIGHT * 1.15),Image.SCALE_FAST);
		
		myController = controller;
		
		//Start/Pause Button
		myStartPauseButton = new JButton("START");
		myStartPauseButton.setFont(myFont3);
		
		;

		//Frame info
		this.setSize(1000,600);
		this.setLocation(0, 0);
		this.setTitle("Trominos");
		this.setResizable(false);
		
		myContentPane = getContentPane();
		myContentPane.setLayout(new BorderLayout());
		
		//Sim Panel
		mySimPanel = new JPanel();
		mySimPanel.setBorder(BorderFactory.createLoweredBevelBorder());
		mySimPanel.setLayout(null);
		
		//Customer Served Counter
		//myTotalServed = new JLabel[MAX_NUM_OF_TELLERS];
		
		/*for(int i = 0; i < myTotalServed.length; i++)
		{
			myTotalServed[i] = new JLabel(" 0");
			myTotalServed[i].setFont(myFont3);
			myTotalServed[i].setBackground(Color.WHITE);
			myTotalServed[i].setSize(COUNTER_BOX_WIDTH, COUNTER_BOX_HEIGHT);
			myTotalServed[i].setLocation(60+(CUSTOMER_WIDTH*i), 30);
			myTotalServed[i].setBorder(BorderFactory.createLoweredBevelBorder());
			mySimPanel.add(myTotalServed[i]);
		}*/

		//Teller locations
		/*myTeller = new JLabel[MAX_NUM_OF_TELLERS];
		myCounter = new JLabel[MAX_NUM_OF_TELLERS];
		
		myScaledCashier = new Image[MAX_NUM_OF_TELLERS];
		
		Image cashier1 = Toolkit.getDefaultToolkit().getImage(TELLER1_IMG);
		myScaledCashier[0] = cashier1.getScaledInstance(BOX_WIDTH,BOX_HEIGHT,Image.SCALE_FAST);
		Image cashier2 = Toolkit.getDefaultToolkit().getImage(TELLER2_IMG);
		myScaledCashier[1] = cashier2.getScaledInstance(BOX_WIDTH,BOX_HEIGHT,Image.SCALE_FAST);
		Image cashier3 = Toolkit.getDefaultToolkit().getImage(TELLER3_IMG);
		myScaledCashier[2] = cashier3.getScaledInstance(BOX_WIDTH,BOX_HEIGHT,Image.SCALE_FAST);
		Image cashier4 = Toolkit.getDefaultToolkit().getImage(TELLER4_IMG);
		myScaledCashier[3] = cashier4.getScaledInstance(BOX_WIDTH,BOX_HEIGHT,Image.SCALE_FAST);
		Image cashier5 = Toolkit.getDefaultToolkit().getImage(TELLER5_IMG);
		myScaledCashier[4] = cashier5.getScaledInstance(BOX_WIDTH,BOX_HEIGHT,Image.SCALE_FAST);
		
		for(int i = 0; i < MAX_NUM_OF_TELLERS; i++)
		{
			;
			myTeller[i] = new JLabel(new ImageIcon(myScaledCashier[i]));
			myTeller[i].setSize(BOX_WIDTH, BOX_HEIGHT);
			myTeller[i].setLocation(20+(CUSTOMER_WIDTH*i), ROW_1);
			myTeller[i].setVisible(true);
			mySimPanel.add(myTeller[i]);
			
			myCounter[i] = new JLabel(new ImageIcon(myScaledCounter));
			myCounter[i].setSize(BOX_WIDTH, BOX_HEIGHT);
			myCounter[i].setLocation((CUSTOMER_WIDTH*i)-10, ROW_1 + 10);
			myCounter[i].setVisible(true);
			mySimPanel.add(myCounter[i]);
		}*/
	
		
		
		//Background
		
		mySidebar = new JPanel(new GridLayout(2,1));
		mySidebar.setBorder(BorderFactory.createLoweredBevelBorder());
		myToolbar = new JPanel(new GridLayout(9,1));
		myToolbar.setSize(100, 500);
		
		
		JLabel bg;
		bg = new JLabel(new ImageIcon("images/background.png"));
		bg.setSize(500, 500);
		bg.setLocation(0, 0);
		mySimPanel.add(bg);
		
		
		
		mySetBoardSizeName.setEditable(false);
		mySetEmptyXName.setEditable(false);
		mySetEmptyYName.setEditable(false);
		
		mySetBoardSizeName.setFont(myFont2);
		mySetEmptyXName.setFont(myFont2);
		mySetEmptyYName.setFont(myFont2);
		
		mySetBoardSizeName.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		mySetEmptyXName.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		mySetEmptyYName.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		
		mySetBoardSizeField.setBorder(BorderFactory.createLoweredBevelBorder());
		mySetEmptyYField.setBorder(BorderFactory.createLoweredBevelBorder());
		
		myToolbar.add(mySetBoardSizeName);
		myToolbar.add(mySetBoardSizeField);
		myToolbar.add(mySetEmptyXName);
		myToolbar.add(mySetEmptyXField);
		myToolbar.add(mySetEmptyYName);
		myToolbar.add(mySetEmptyYField);
		myToolbar.add(myStartPauseButton);
		
		JPanel cashierPanel = new JPanel(new GridLayout(1,5));
		Dimension dimension = new Dimension(50,50);
		cashierPanel.setMinimumSize(dimension);
		//myCashierDisplays = new JTextArea[MAX_NUM_OF_TELLERS];
		/*for (int i = 0; i < MAX_NUM_OF_TELLERS; i++)
		{
			myCashierDisplays[i] = new JTextArea("CASHIER STATISTICS: \n \n \n \n \n \n");
			myCashierDisplays[i].setFont(myFont2);
			myCashierDisplays[i].setEditable(false);
			myCashierDisplays[i].setBorder(BorderFactory.createLoweredBevelBorder());
			cashierPanel.add(myCashierDisplays[i]);
		}*/
		
		JPanel infoArea = new JPanel(new GridLayout());
		mySidebar.add(myToolbar);
		//infoArea.add(myOverallDisplay);
		//mySidebar.add(infoArea);
		myOverSimPanel = new JPanel(new BorderLayout());
		myOverSimPanel.add(mySimPanel, BorderLayout.CENTER);
		myOverSimPanel.add(cashierPanel, BorderLayout.SOUTH);
		myContentPane.add(myOverSimPanel, BorderLayout.CENTER);
		myContentPane.add(mySidebar, BorderLayout.EAST);
		
		
		
		
		this.associateListeners(myController);
		
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);	
	}
	
	//////////////////////////////////////////
	//            Methods                   //
	//////////////////////////////////////////

	/**
	 * Changes the button text from start to resume to pause depending on which is needed
	 * @author hudson
	 */
	public void changeStartPause() 
	{
		if(myStartPauseButton.getText().equals("START"))
		{
			myStartPauseButton.setText("RUNNING");
		}
		else
		{
			if(myController.getModel() != null)
			{
				myStartPauseButton.setText("RUNNING");
			} else myStartPauseButton.setText("STOPPED");
		}
	}
	
	public void setDisplaySize()
	{
		int size = myController.myBoxSize;
		double sizeChange = (500/size);
		
		myScaledEmptyBox = emptyBox.getScaledInstance((int)sizeChange,(int)sizeChange,Image.SCALE_FAST);
		myScaledFullBox = fullBox.getScaledInstance((int)sizeChange,(int)sizeChange,Image.SCALE_FAST);
		
		myBoxes = new JLabel[size][size];
				for(int i = 0; i < size; i++)
				{
					for(int j = 0; j < size; j++)
					{
						myBoxes[i][j] = new JLabel();
						myBoxes[i][j].setSize((int)sizeChange, (int)sizeChange);
						myBoxes[i][j].setLocation((int)(sizeChange*i), (int)(sizeChange*j));
						myBoxes[i][j].setVisible(true);
						mySimPanel.add(myBoxes[i][j]);
					}
				}
	}
	
	 public void setBoxes()
	 {
		
		/*for(int i = 0; i < Trom.getMyBoxSize(); i++)
		{
			for(int j = 0; j < Trom.getMyBoxSize(); j++)
			{
				myBoxes[i][j].setVisible(false);
			}
		}*/
		try
		{
			for(int i = 0; i < myController.myBoxSize; i++)
			{
				for(int j = 0; j < myController.myBoxSize; j++)
				{
				
					myBoxes[i][j].setVisible(true);
					if(myController.myBox[i][j])
					{
						myBoxes[i][j].setIcon(new ImageIcon(myScaledFullBox));
					} else myBoxes[i][j].setIcon(new ImageIcon(myScaledEmptyBox));
				}
			}
		}
		catch (NullPointerException e)
		{
			System.out.println("Error in setting boxes");
		}
	 }
	 
	 
	 
	/**
	 * Associates the button with the appropriate method
	 * @author hudson
	 * @param controller The controller in which the method is included.
	 */
	private void associateListeners(TromController controller)
	{
		Class<? extends TromController> controllerClass;
		Method startPauseMethod;
		
		controllerClass = myController.getClass();
				
		startPauseMethod = null;
		try 
		{
			startPauseMethod = 
				controllerClass.getMethod("startPause", (Class<?>[])null);
		} 
		catch (SecurityException e) 
		{	
			String error;

			error = e.toString();
			System.out.println(error);
		}  
		catch (NoSuchMethodException e) 
		{
			String error;

	        error = e.toString();
	        System.out.println(error);
		}


		myStartPauseListener = 
			new ButtonListener(myController, startPauseMethod, null);
				
		myStartPauseButton.addMouseListener(myStartPauseListener);
		
	}
	
	public int getBoxSize()
	{
		String word = mySetBoardSizeField.getText();
		if (word != null && word != "")
		{
			if (word.length() <= 0) return -1;
			for (int i = 0; i < word.length(); i++) 
			{
				if (!Character.isDigit(word.charAt(i)))
				{
					return -1;
				}
			}
		} else return -1;
		int pow = Integer.parseInt(word);
		
		if ((pow > 0) && ((pow & (pow - 1)) == 0))
		{ 
			return Integer.parseInt(word);
		} else return -1;
	}

	public int getThisX()
	{
		String word = mySetEmptyXField.getText();
		if (word != null && word != "")
		{
			if (word.length() <= 0) return -1;
			for (int i = 0; i < word.length(); i++) 
			{
				if (!Character.isDigit(word.charAt(i)))
				{
					return -1;
				}
			}
		} else return -1;
		
		
		return Integer.parseInt(word);
	}
	
	public int getThisY()
	{
		String word = mySetEmptyYField.getText();
		if (word != null && word != "")
		{
			if (word.length() <= 0) return -1;
			for (int i = 0; i < word.length(); i++) 
			{
				if (!Character.isDigit(word.charAt(i)))
				{
					return -1;
				}
			}
		} else return -1;
		
		
		return Integer.parseInt(word);
	}
	
	/**
	 * Resets View to initial text values
	 * @author hudson
	 */
	public void resetView()
	{
		mySetEmptyXField.setText(null);
		mySetEmptyYField.setText(null);
		mySetBoardSizeField.setText(null);
		myStartPauseButton.setText("STOPPED");
	}

}

